'use strict'

/**
 * Написати реалізацію кнопки "Показати пароль". Завдання має бути виконане на чистому Javascript
 * без використання бібліотек типу jQuery або React.
 *
 * Технічні вимоги:
 * У файлі index.html лежить розмітка двох полів вводу пароля.
 * Після натискання на іконку поруч із конкретним полем - повинні відображатися символи, які ввів користувач,
 * іконка змінює свій зовнішній вигляд.
 * У коментарях під іконкою - інша іконка, саме вона повинна відображатися замість поточної.
 * Коли пароля не видно - іконка поля має виглядати як та, що в першому полі (Ввести пароль)
 * Коли натиснута іконка, вона має виглядати, як та, що у другому полі (Ввести пароль)
 * Натиснувши кнопку Підтвердити, потрібно порівняти введені значення в полях
 * Якщо значення збігаються – вивести модальне вікно (можна alert) з текстом – You are welcome;
 * Якщо значення не збігаються - вивести під другим полем текст червоного кольору Потрібно ввести однакові значення
 * Після натискання на кнопку сторінка не повинна перезавантажуватись
 * Можна міняти розмітку, додавати атрибути, теги, id, класи тощо. *
 * */

/**
 * 1st version, shit code, bud works :)
 * Needs to uncomment <i>-tags wih fa-eye-slash class
 * */

/*
window.addEventListener('load', () => {
    document.querySelectorAll('.fa-eye-slash').forEach(item => {
        item.style.display = 'none';
        item.addEventListener('click', function() {
            this.style.display = 'none';
            this.previousElementSibling.style.display = 'block';
            this.previousElementSibling.previousElementSibling.type = 'password';
        })
    })
    document.querySelectorAll('.fa-eye').forEach(item => {
        item.addEventListener('click', function() {
            this.style.display = 'none';
            this.nextElementSibling.style.display = 'block';
            this.previousElementSibling.type = 'text';
        })
    })
})

function passCompare() {
    return document.querySelectorAll('input')[0].value ? (document.querySelectorAll('input')[0].value === document.querySelectorAll('input')[1].value) : false;
}

document.querySelector('form').addEventListener('submit', function(ev) {
    ev.preventDefault();
    document.querySelector('span') ? document.querySelector('span').remove() : false;
    ev.preventDefault();
    if (!passCompare()) {
        document.querySelector('.btn').insertAdjacentHTML('beforebegin', '<span>Password empty or not equal!</span>');
    } else {
        document.querySelector('.btn').insertAdjacentHTML('beforebegin', '<span>You are welcome!</span>');
        this.submit();
    }
})
*/

window.addEventListener('load', () => {
    let pass = document.querySelectorAll('input');
    let icons = document.querySelectorAll('.fas');

    icons.forEach(el => el.addEventListener('click', function () {
        if (el.classList.contains('fa-eye')) {
            el.classList.remove('fa-eye');
            el.classList.add('fa-eye-slash');
            el.previousElementSibling.type = 'text';
        } else {
            el.classList.remove('fa-eye-slash');
            el.classList.add('fa-eye');
            el.previousElementSibling.type = 'password';
        }
    }))

    document.querySelector('form').addEventListener('submit', function (event) {
        event.preventDefault();
        document.querySelector('#msg') ? document.querySelector('#msg').remove() : false;
        let message = function (str) {
            document.querySelector('button').insertAdjacentHTML('beforebegin', `<div id="msg">${str}</div>`)
        };
        return (!pass[0].value || pass[0].value !== pass[1].value) ? message('Password is empty or not equal!') : message('You are welcome!'); this.submit();
    })
})














